// Avoid `console` errors in browsers that lack a console.
(function() {
  var method;
  var noop = function () {};
  var methods = [
    'assert', 'clear', 'count', 'debug', 'dir', 'dirxml', 'error',
    'exception', 'group', 'groupCollapsed', 'groupEnd', 'info', 'log',
    'markTimeline', 'profile', 'profileEnd', 'table', 'time', 'timeEnd',
    'timeline', 'timelineEnd', 'timeStamp', 'trace', 'warn'
  ];
  var length = methods.length;
  var console = (window.console = window.console || {});

  while (length--) {
    method = methods[length];

    // Only stub undefined methods.
    if (!console[method]) {
      console[method] = noop;
    }
  }
}());

// Place any jQuery/helper plugins in here.
//navbar
$(document).ready(function() {
  //menu
  $('#menu').load('templates/menu/menu.html');
  $('#menu-sub').load('templates/menu/menu-sub.html');

  //plugins
  //carga tienda
  window.tienda = function (x) {
    var nav = x;
    window.location.href = 'tienda.php?tie=' + nav
  }
  //carga producto
  window.producto = function (x,y) {
    var tie = x;
    var pro = y
    window.location.href = 'producto.php?tie=' + tie +'&pro=' + pro
  }
  //plug ins
  //CREA UN TOAST MODAL
  window.toast = function(e) {
    if ($('#toast').length) {
      $('#toast').remove();
    }
    var toast = '<span id="toast" class="tag is-dark tag-toast">' + e + '</span>';
    $('body').append(toast);
    setTimeout(function(){
      if ($('#toast').length > 0) {
        $('#toast').fadeOut('fast', function() {
          $(this).remove()
        })
      }
    }, 4000)
  }
  //MODAL SOLO CLAUSURABLE CON BOTON
  window.modal = function(incoming) {
    //DESTRUYE VIEJOS MODALES
    if ($('.modal').length) {
      $('.modal').fadeOut('fast', function() {
        $(this).remove()
      })
    }
    //CREA UN MODAL NUEVO
    var content = incoming;
    var modal = '<div class="modal">';
      modal += '<div class="modal-background"></div>';
      modal += '<div class="modal-content">' + content + '</div>';
      modal += '<button class="modal-close is-large" aria-label="close"></button>';
    modal += '</div>';
    $('body').prepend(modal);
    $('.modal').css('z-index','9000');
    $('.modal').toggleClass("is-active");
    $('.modal-close').on('click', function(){
      $('.modal').fadeOut('fast', function() {
        $(this).remove()
      })
    })
  }
  //CIERRAMODAL
  window.xmodal = function(e) {
    //DESTRUYE VIEJOS MODALES
    if ($('.modal').length) {
      $('.modal').fadeOut('fast', function() {
        $(this).remove()
      })
    }
  }
  //get variables from url
  window.getUrlGet = function(sParam) {
    var sPageURL = window.location.search.substring(1),
      sURLVariables = sPageURL.split('&'),
      sParameterName,
      i;

    for (i = 0; i < sURLVariables.length; i++) {
      sParameterName = sURLVariables[i].split('=');

      if (sParameterName[0] === sParam) {
        return sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
      }
    }
  }
});
