$(document).ready(function() {
  //carga carrito
  window.carrito = function (returnContainer) {
    var content;
    var stringaUth_key = localStorage.getItem("aUth_key");
    var stringaUth_user = localStorage.getItem("aUth_user");
    var form_data = new FormData();
    form_data.append('auth',stringaUth_key);
    form_data.append('user', stringaUth_user);
    $.ajax({
      type: 'POST',
      url: 'php/carrito/carrito-carga-actual.php',
      data: form_data,
      cache: false,
      contentType: false,
      processData: false,
      async: false,
      success: function(data) {
        $.each(data, function (name, value) {
          if (value.success) {
            content = value.carrito
          } else {
            toast(value.message)
          }
        })
      },
      error: function(xhr, tst, err) {
        toast(err)
      }
    })
    return content
  }
  //agregar productos al carrito
  window.carrito_agr = function (tie, pro) {
    var stringaUth_key = localStorage.getItem("aUth_key");
    var stringaUth_user = localStorage.getItem("aUth_user");
    var tienda = tie;
    var producto = pro;
    var form_data = new FormData();
    form_data.append('auth',stringaUth_key);
    form_data.append('user', stringaUth_user);
    form_data.append('tienda', tienda);
    form_data.append('producto', producto);
    $.ajax({
      type: 'POST',
      url: 'php/carrito/carrito-agrega.php',
      data: form_data,
      cache: false,
      contentType: false,
      processData: false,
      async: false,
      success: function(data) {
        var content = '';
          $.each(data, function (name, value) {
            if (value.success) {
              toast(value.message)
            } else {
              toast(value.message)
            }
        })
      },
      error: function(xhr, tst, err) {
        toast(err)
      }
    })
  }
})
