-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 26-09-2020 a las 00:43:48
-- Versión del servidor: 10.4.13-MariaDB
-- Versión de PHP: 7.4.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `tienda_bdd`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `auth_table`
--

CREATE TABLE `auth_table` (
  `id` int(11) NOT NULL COMMENT 'id de la tabla',
  `init_index` text COLLATE utf8mb4_spanish2_ci NOT NULL COMMENT 'init index',
  `nom` text COLLATE utf8mb4_spanish2_ci NOT NULL COMMENT 'usuario de login',
  `pas` text COLLATE utf8mb4_spanish2_ci NOT NULL COMMENT 'password de usuario',
  `id_usr` text COLLATE utf8mb4_spanish2_ci NOT NULL COMMENT 'id del usuario'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish2_ci;

--
-- Volcado de datos para la tabla `auth_table`
--

INSERT INTO `auth_table` (`id`, `init_index`, `nom`, `pas`, `id_usr`) VALUES
(1, 'Wd6SuFrl', 'beto', '$2y$10$S4EjwyciShDvbwgh7rB41uRNUvRe.PzG322d4FBSvA9LLGnij3mHG', 'rHRJFClp');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `carr_table`
--

CREATE TABLE `carr_table` (
  `id` int(11) NOT NULL COMMENT 'id de la tabla',
  `id_tra` text COLLATE utf8mb4_spanish2_ci NOT NULL COMMENT 'id de la transaccion',
  `id_car` text COLLATE utf8mb4_spanish2_ci NOT NULL COMMENT 'id del carrito',
  `id_pro` text COLLATE utf8mb4_spanish2_ci NOT NULL COMMENT 'id del producto',
  `id_tie` text COLLATE utf8mb4_spanish2_ci NOT NULL COMMENT 'id de la tienda',
  `v_per` text COLLATE utf8mb4_spanish2_ci NOT NULL COMMENT 'id del vendedor',
  `c_per` text COLLATE utf8mb4_spanish2_ci NOT NULL COMMENT 'id del comprador',
  `pre` decimal(13,2) NOT NULL COMMENT 'cantidad de la transaccion',
  `fec` datetime NOT NULL COMMENT 'fecha de la transaccion'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish2_ci;

--
-- Volcado de datos para la tabla `carr_table`
--

INSERT INTO `carr_table` (`id`, `id_tra`, `id_car`, `id_pro`, `id_tie`, `v_per`, `c_per`, `pre`, `fec`) VALUES
(1, 'FRdtEGqz_TRA', 'zw5lM6Kd_CAR', 'HIFzrrIX_PRO', 'Dllphyfv_TIE', 'KOTHXAsd_PER', 'KOTHXAsd_PER', '1234.00', '2020-09-11 16:38:50'),
(2, '08aCaMcS_TRA', 'zw5lM6Kd_CAR', 'HIFzrrIX_PRO', 'Dllphyfv_TIE', 'KOTHXAsd_PER', 'KOTHXAsd_PER', '1234.00', '2020-09-11 16:39:01'),
(3, 'K3bGHaIC_TRA', 'zw5lM6Kd_CAR', '8ZZbDNKB_PRO', 'Dllphyfv_TIE', 'KOTHXAsd_PER', 'KOTHXAsd_PER', '222.00', '2020-09-11 16:39:02'),
(4, 'EP4P1GDM_TRA', 'zw5lM6Kd_CAR', 'HIFzrrIX_PRO', 'Dllphyfv_TIE', 'KOTHXAsd_PER', 'KOTHXAsd_PER', '1234.00', '2020-09-11 16:40:52'),
(5, '7FZRho6A_TRA', 'zw5lM6Kd_CAR', 'HIFzrrIX_PRO', 'Dllphyfv_TIE', 'KOTHXAsd_PER', 'KOTHXAsd_PER', '1234.00', '2020-09-11 18:34:45'),
(6, 'rZYUuwfM_TRA', 'zw5lM6Kd_CAR', 'HIFzrrIX_PRO', 'Dllphyfv_TIE', 'KOTHXAsd_PER', 'KOTHXAsd_PER', '1234.00', '2020-09-11 18:36:39'),
(7, 'UCniK8My_TRA', 'zw5lM6Kd_CAR', '8ZZbDNKB_PRO', 'Dllphyfv_TIE', 'KOTHXAsd_PER', 'KOTHXAsd_PER', '222.00', '2020-09-11 18:37:37'),
(8, 'InqowWql_TRA', 'zw5lM6Kd_CAR', 'HIFzrrIX_PRO', 'Dllphyfv_TIE', 'KOTHXAsd_PER', 'KOTHXAsd_PER', '1234.00', '2020-09-11 18:37:58'),
(9, 'YgjAtqXE_TRA', 'zw5lM6Kd_CAR', '8ZZbDNKB_PRO', 'Dllphyfv_TIE', 'KOTHXAsd_PER', 'KOTHXAsd_PER', '222.00', '2020-09-14 11:23:01'),
(10, 'TghohMTx_TRA', 'zw5lM6Kd_CAR', '8ZZbDNKB_PRO', 'Dllphyfv_TIE', 'KOTHXAsd_PER', 'KOTHXAsd_PER', '222.00', '2020-09-14 11:23:36'),
(11, '1u09Vl51_TRA', 'zw5lM6Kd_CAR', '8ZZbDNKB_PRO', 'Dllphyfv_TIE', 'KOTHXAsd_PER', 'KOTHXAsd_PER', '222.00', '2020-09-14 17:58:41'),
(12, 'VGOQSGu8_TRA', 'zw5lM6Kd_CAR', 'HIFzrrIX_PRO', 'Dllphyfv_TIE', 'KOTHXAsd_PER', 'KOTHXAsd_PER', '1234.00', '2020-09-14 17:58:41'),
(13, '6Ejzxbnc_TRA', 'zw5lM6Kd_CAR', 'HIFzrrIX_PRO', 'Dllphyfv_TIE', 'KOTHXAsd_PER', 'KOTHXAsd_PER', '1234.00', '2020-09-14 17:58:42'),
(14, 'uwIP3VSR_TRA', 'zw5lM6Kd_CAR', '8ZZbDNKB_PRO', 'Dllphyfv_TIE', 'KOTHXAsd_PER', 'KOTHXAsd_PER', '222.00', '2020-09-14 17:58:42'),
(15, 'Cx4leQ92_TRA', 'zw5lM6Kd_CAR', '8ZZbDNKB_PRO', 'Dllphyfv_TIE', 'KOTHXAsd_PER', 'KOTHXAsd_PER', '23.22', '2020-09-14 18:49:54'),
(16, 'B7ilVd7q_TRA', 'zw5lM6Kd_CAR', '8ZZbDNKB_PRO', 'Dllphyfv_TIE', 'KOTHXAsd_PER', 'KOTHXAsd_PER', '23.22', '2020-09-14 19:26:02'),
(17, '2saHF1j1_TRA', 'zw5lM6Kd_CAR', '8ZZbDNKB_PRO', 'Dllphyfv_TIE', 'KOTHXAsd_PER', 'KOTHXAsd_PER', '23.22', '2020-09-19 10:21:51'),
(18, 'BWoT8DWM_TRA', 'zw5lM6Kd_CAR', '8ZZbDNKB_PRO', 'Dllphyfv_TIE', 'KOTHXAsd_PER', 'KOTHXAsd_PER', '23.22', '2020-09-22 17:59:23');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `dire_table`
--

CREATE TABLE `dire_table` (
  `id` int(11) NOT NULL COMMENT 'id de la tabla',
  `cale` text COLLATE utf8mb4_spanish2_ci NOT NULL COMMENT 'calle',
  `num` text COLLATE utf8mb4_spanish2_ci NOT NULL COMMENT 'numero',
  `col` text COLLATE utf8mb4_spanish2_ci NOT NULL COMMENT 'colonia',
  `cp` mediumint(9) NOT NULL COMMENT 'codigo postal',
  `ciu` text COLLATE utf8mb4_spanish2_ci NOT NULL COMMENT 'ciudad',
  `est` text COLLATE utf8mb4_spanish2_ci NOT NULL COMMENT 'estado',
  `pai` text COLLATE utf8mb4_spanish2_ci NOT NULL COMMENT 'pais',
  `ref` text COLLATE utf8mb4_spanish2_ci NOT NULL COMMENT 'notas',
  `id_per` text COLLATE utf8mb4_spanish2_ci NOT NULL COMMENT 'id del perfil'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish2_ci;

--
-- Volcado de datos para la tabla `dire_table`
--

INSERT INTO `dire_table` (`id`, `cale`, `num`, `col`, `cp`, `ciu`, `est`, `pai`, `ref`, `id_per`) VALUES
(1, '', '', '', 0, '', '', '', '', 'KOTHXAsd_PER');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `perf_table`
--

CREATE TABLE `perf_table` (
  `id` int(11) NOT NULL COMMENT 'id de la table',
  `id_usr` text COLLATE utf8mb4_spanish2_ci NOT NULL COMMENT 'id del perfil',
  `nom` text COLLATE utf8mb4_spanish2_ci NOT NULL,
  `ape` text COLLATE utf8mb4_spanish2_ci NOT NULL,
  `apm` text COLLATE utf8mb4_spanish2_ci NOT NULL,
  `tel` text COLLATE utf8mb4_spanish2_ci NOT NULL,
  `cel` text COLLATE utf8mb4_spanish2_ci NOT NULL,
  `mai` text COLLATE utf8mb4_spanish2_ci NOT NULL COMMENT 'mail del usr',
  `fec` float NOT NULL COMMENT 'fecha de creacion de perfil',
  `id_per` text COLLATE utf8mb4_spanish2_ci NOT NULL COMMENT 'id del perfil'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish2_ci;

--
-- Volcado de datos para la tabla `perf_table`
--

INSERT INTO `perf_table` (`id`, `id_usr`, `nom`, `ape`, `apm`, `tel`, `cel`, `mai`, `fec`, `id_per`) VALUES
(1, 'rHRJFClp', '', '', '', '', '', 'm@il.com', 2020, 'KOTHXAsd_PER');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `prod_table`
--

CREATE TABLE `prod_table` (
  `id` int(11) NOT NULL COMMENT 'id de la tabla',
  `id_pro` text COLLATE utf8mb4_spanish2_ci NOT NULL COMMENT 'id del producto',
  `fec` datetime NOT NULL COMMENT 'fecha de creacion de producto',
  `id_prp` text COLLATE utf8mb4_spanish2_ci NOT NULL COMMENT 'id de las preferencias del producto',
  `id_tie` text COLLATE utf8mb4_spanish2_ci NOT NULL COMMENT 'id de la tienda\r\n'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish2_ci;

--
-- Volcado de datos para la tabla `prod_table`
--

INSERT INTO `prod_table` (`id`, `id_pro`, `fec`, `id_prp`, `id_tie`) VALUES
(5, '8ZZbDNKB_PRO', '2020-09-09 17:06:17', '', 'Dllphyfv_TIE'),
(6, 'HIFzrrIX_PRO', '2020-09-10 17:29:15', '', 'Dllphyfv_TIE');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tien_table`
--

CREATE TABLE `tien_table` (
  `id` int(11) NOT NULL COMMENT 'id de la tabla',
  `nom` text COLLATE utf8mb4_spanish2_ci NOT NULL COMMENT 'nombre de la tienda',
  `fec` datetime NOT NULL COMMENT 'fecha de creacion de tienda',
  `id_per` text COLLATE utf8mb4_spanish2_ci NOT NULL COMMENT 'id del perfil',
  `id_tie` text COLLATE utf8mb4_spanish2_ci NOT NULL COMMENT 'id de la tienda'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish2_ci;

--
-- Volcado de datos para la tabla `tien_table`
--

INSERT INTO `tien_table` (`id`, `nom`, `fec`, `id_per`, `id_tie`) VALUES
(14, 'Tiendaton', '2020-09-09 15:37:57', 'KOTHXAsd_PER', 'Dllphyfv_TIE');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `vent_table`
--

CREATE TABLE `vent_table` (
  `id` int(11) NOT NULL COMMENT 'indice de la tabla',
  `id_car` text COLLATE utf8mb4_spanish2_ci NOT NULL COMMENT 'id del carrito de compra',
  `fec` datetime NOT NULL COMMENT 'fecha de la transacción',
  `tot` decimal(13,2) NOT NULL COMMENT 'total de la transacción'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish2_ci;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `auth_table`
--
ALTER TABLE `auth_table`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `carr_table`
--
ALTER TABLE `carr_table`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `dire_table`
--
ALTER TABLE `dire_table`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `perf_table`
--
ALTER TABLE `perf_table`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `prod_table`
--
ALTER TABLE `prod_table`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `tien_table`
--
ALTER TABLE `tien_table`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `vent_table`
--
ALTER TABLE `vent_table`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `auth_table`
--
ALTER TABLE `auth_table`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id de la tabla', AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `carr_table`
--
ALTER TABLE `carr_table`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id de la tabla', AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT de la tabla `dire_table`
--
ALTER TABLE `dire_table`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id de la tabla', AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `perf_table`
--
ALTER TABLE `perf_table`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id de la table', AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `prod_table`
--
ALTER TABLE `prod_table`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id de la tabla', AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `tien_table`
--
ALTER TABLE `tien_table`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id de la tabla', AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT de la tabla `vent_table`
--
ALTER TABLE `vent_table`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'indice de la tabla';
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
