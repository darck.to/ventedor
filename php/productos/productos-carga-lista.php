<?php
  session_start();
  header("Access-Control-Allow-Origin: *");
	header('Content-type: application/json');
  include_once('../../functions/productos_functions.php');
  include_once('../../functions/abre_conexion.php');

  //$nav = mysqli_real_escape_string($mysqli,$_POST['nav']);
  $auth = mysqli_real_escape_string($mysqli,$_POST['auth']);
  $user = mysqli_real_escape_string($mysqli,$_POST['user']);

  $resultados = array();

  $sql = $mysqli->query("SELECT id_usr FROM auth_table WHERE init_index = '".$auth."' AND nom = '".$user."' ");
  if ($sql->num_rows > 0) {
    $row = $sql->fetch_assoc();
    $sqp = $mysqli->query("SELECT id_per FROM perf_table WHERE id_usr = '".$row['id_usr']."'");
    if ($sqp->num_rows > 0) {
      $rop = $sqp->fetch_assoc();
      $sqt = $mysqli->query("SELECT id_tie FROM tien_table WHERE id_per = '".$rop['id_per']."'");
      if ($sqt->num_rows > 0) {
        while ($rot = $sqt->fetch_assoc()) {
          $sqpr = $mysqli->query("SELECT id_pro FROM prod_table WHERE id_tie = '".$rot['id_tie']."'");
          if ($sqpr->num_rows > 0) {
            while ($rpr = $sqpr->fetch_assoc()) {
              $name[] = array("key"=> "nombre");
              $nombre = productos_obtener_value($rop['id_per'], $rot['id_tie'], $rpr['id_pro'], $name);
              $price[] = array("key"=> "precio");
              $precio = productos_obtener_value($rop['id_per'], $rot['id_tie'], $rpr['id_pro'], $price);
              $resultados[] = array("success"=> true, "nombre"=> $nombre, "precio"=> $precio, "id_pro" => $rpr['id_pro'], "id_tie" => $rot['id_tie']);
            }
          }
        }
      }
    }
  } else {
    $resultados[] = array("success"=> false, "message" => "Auth error");
  }
  print json_encode($resultados);
  include_once('../../functions/cierra_conexion.php');
?>
