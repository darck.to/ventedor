<?php
  session_start();
  header("Access-Control-Allow-Origin: *");
	header('Content-type: application/json');
  include_once('../../functions/functions.php');
  include_once('../../functions/productos_functions.php');
  include_once('../../functions/abre_conexion.php');

  //$nav = mysqli_real_escape_string($mysqli,$_POST['nav']);
  $auth = mysqli_real_escape_string($mysqli,$_POST['auth']);
  $user = mysqli_real_escape_string($mysqli,$_POST['user']);

  date_default_timezone_set("America/Mexico_City");
  $fechaActual = Date('Y-m-d H:i:s');

  $resultados = array();

  $sql = $mysqli->query("SELECT id_usr FROM auth_table WHERE init_index = '".$auth."' AND nom = '".$user."' ");
  if ($sql->num_rows > 0) {
    $row = $sql->fetch_assoc();
    $sql = $mysqli->query("SELECT id_per FROM perf_table WHERE id_usr = '".$row['id_usr']."'");
    //lee usuario
    if ($sql->num_rows > 0) {
      $row = $sql->fetch_assoc();
      $usuario = $row['id_per'];
      //lee accion entrante
      $action = mysqli_real_escape_string($mysqli,$_POST['action']);
      if ($action == 0) {
        $nombre = mysqli_real_escape_string($mysqli,$_POST['nombre']);
        $precio = mysqli_real_escape_string($mysqli,$_POST['precio']);
        $tienda = mysqli_real_escape_string($mysqli,$_POST['tienda']);
        $id_pr = generateRandomString(8) . "_PRO";
        if ($sql = $mysqli->query("INSERT INTO prod_table (fec, id_pro, id_tie) VALUES ('".$fechaActual."', '".$id_pr."', '".$tienda."')")) {
          //si se crea la esctructura de archivos de la tienda, puede proseguir, si no borra la tienda de la bdd
          if (productos_crea_estructura($usuario, $tienda, $id_pr)) {
            $content[] = array("id_tie"=> $tienda, "id_pro"=> $id_pr, "nombre"=> $nombre, "precio"=> $precio, "fecha_m"=> $fechaActual);
            if (productos_configuracion_json($action, $usuario, $tienda, $id_pr, $content)) {
              $resultados[] = array("success"=> true, "message"=> "Alta de producto");
            }
          } else {
            if ($mysqli->query("DELETE FROM prod_table WHERE id_pro = '".$id_pr."' AND id_tie = '".$tienda."'")) {
              $resultados[] = array("success"=> false, "message"=> "Falló creación de estructura en alta de producto, se eliminó el registro");
            }
          }
        } else {
          $resultados[] = array("success"=> false, "message"=> "Falló alta de producto");
        }
      } else if ($action == 1) {
        $name = mysqli_real_escape_string($mysqli,$_POST['nombre']);
        $price = mysqli_real_escape_string($mysqli,$_POST['precio']);
        $value = mysqli_real_escape_string($mysqli,$_POST['value']);
        $tienda = mysqli_real_escape_string($mysqli,$_POST['tienda']);
        $nombre[] = array("key"=> "nombre", "value"=> $name);
        if (productos_configuracion_json($action, $usuario, $tienda, $value, $nombre)) {
          $resultados[] = array("success"=> true, "message"=> "Modificación de nombre de producto");
        } else {
          $resultados[] = array("success"=> false, "message"=> "Falló modificación de producto");
        }
        $precio[] = array("key"=> "precio", "value"=> $price);
        if (productos_configuracion_json($action, $usuario, $tienda, $value, $precio)) {
          $resultados[] = array("success"=> true, "message"=> "Modificación de precio de producto");
        } else {
          $resultados[] = array("success"=> false, "message"=> "Falló modificación de producto");
        }
      } else if ($action == 2) {
        $value = mysqli_real_escape_string($mysqli,$_POST['value']);
        if ($sql = $mysqli->query("SELECT id_tie FROM prod_table WHERE id_pro = '".$value."'")) {
          if ($sql->num_rows > 0) {
            $row = $sql->fetch_assoc();
            $tienda = $row['id_tie'];
            if (productos_borra_estructura($usuario, $tienda, $value)) {
              if ($sql = $mysqli->query("DELETE FROM prod_table WHERE id_pro = '".$value."'")) {
                $resultados[] = array("success"=> true, "message"=> "Producto eliminado");
              } else {
                $resultados[] = array("success"=> false, "message"=> "Falló la eliminación de producto");
              }
            } else {
              $resultados[] = array("success"=> false, "message"=> "Falló la eliminación de le estructura de directorios del producto");
            }
          }
        } else {
          $resultados[] = array("success"=> false, "message"=> "No se encontro el registro del producto");
        }
      }
    }
  } else {
    $resultados[] = array("success"=> false, "message" => "Auth error");
  }

  print json_encode($resultados);
  include_once('../../functions/cierra_conexion.php');
?>
