<?php
  header("Access-Control-Allow-Origin: *");
  header('Content-type: application/json');
  include_once('../../functions/abre_conexion.php');
  include_once('../../functions/functions.php');
  include_once('../../functions/perfiles_functions.php');

  date_default_timezone_set("America/Mexico_City");
  $fechaActual = Date('Y-m-d H:i:s');
  $localIP = getHostByName(getHostName());

  $resultados = array();

  // "limpiamos" los campos del formulario de posibles códigos maliciosos
  $usuario_nombre = mysqli_real_escape_string($mysqli, $_POST['nom']);
  $usuario_email = mysqli_real_escape_string($mysqli, $_POST['mai']);
  $usuario_clave = mysqli_real_escape_string($mysqli, $_POST['pas']);

  $init_index = generateRandomString(8);
  $id_usr = generateRandomString(8);
  $id_per = generateRandomString(8) . "_PER";
  $id_pt = generateRandomString(8) . "_TIE";

  // comprobamos que el usuario ingresado no haya sido registrado antes
  $sql = $mysqli->query("SELECT nom FROM auth_table WHERE nom ='".$usuario_nombre."'");
  if ($sql->num_rows > 0) {
    $resultados[] = array("success"=> false, "type"=> "register", "ip"=> $localIP, "date"=> $fechaActual, "error"=> "El usuario ya ha sido registrado previamente");
  } else {
    $sql = $mysqli->query("SELECT mai FROM perf_table WHERE mai ='".$usuario_email."'");
    if ($sql->num_rows > 0) {
      $resultados[] = array("success"=> false, "type"=> "register", "ip"=> $localIP, "date"=> $fechaActual, "error"=> "El correo ya ha sido registrado previamente");
    } else {
      $usuario_clave = password_hash($usuario_clave, PASSWORD_BCRYPT); // encriptamos la contraseña ingresada con md5
      // ingresamos los datos a la BD TLABLA INDEX
      if(perfiles_crea_estrutura($id_per, $id_pt)) {
        $sqlReg = $mysqli->query("INSERT INTO auth_table (nom, pas, init_index, id_usr) VALUES ('".$usuario_nombre."', '".$usuario_clave."', '".$init_index."', '".$id_usr."')");
        if($sqlReg) {
            //GUARDAMOS EL PERFIL
            if ($sqlPerf = $mysqli->query("INSERT INTO perf_table (id_usr, mai, id_per, fec) VALUES ('".$id_usr."', '".$usuario_email."', '".$id_per."', '".$fechaActual."')")) {
              //GUARDAMOS LA DIRECCION DE ENVIO
              if ($sqlPerf = $mysqli->query("INSERT INTO dire_table (id_per) VALUES ('".$id_per."')")) {
              //GUARDAMOS LA TIENDA
              if ($sqlTien = $mysqli->query("INSERT INTO tien_table (id_per,id_tie, fec) VALUES ('".$id_per."', '".$id_pt."', '".$fechaActual."')")) {
                //RETROALIMENTACIÓN
                $resultados[] = array("success"=> true, "type"=> "register", "ip"=> $localIP, "date"=> $fechaActual, "aUth_key"=> $init_index, "aUth_user"=> $usuario_nombre);
              }
            }
        } else {
          $resultados[] = array("success"=> false, "type"=> "register", "ip"=> $localIP, "date"=> $fechaActual, "error"=> "Error, contact support");
          //$resultados[] = array("success"=> false, "error"=> mysqli_error($mysqli));
        }
      } else {
        $resultados[] = array("success"=> false, "type"=> "estructure/register", "ip"=> $localIP, "date"=> $fechaActual, "aUth_key"=> $init_index, "aUth_user"=> $usuario_nombre);
      }
    }
  }

  print json_encode($resultados);
  // incluimos el archivo de desconexion a la Base de Datos
  include('../../functions/cierra_conexion.php');
?>
