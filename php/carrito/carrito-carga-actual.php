<?php
  session_start();
  header("Access-Control-Allow-Origin: *");
	header('Content-type: application/json');
  include_once('../../functions/carrito_functions.php');
  include_once('../../functions/abre_conexion.php');

  //$nav = mysqli_real_escape_string($mysqli,$_POST['nav']);
  $auth = mysqli_real_escape_string($mysqli,$_POST['auth']);
  $user = mysqli_real_escape_string($mysqli,$_POST['user']);

  $resultados = array();

  $sql = $mysqli->query("SELECT id_usr FROM auth_table WHERE init_index = '".$auth."' AND nom = '".$user."' ");
  if ($sql->num_rows > 0) {
    $row = $sql->fetch_assoc();
    $sql = $mysqli->query("SELECT id_per FROM perf_table WHERE id_usr = '".$row['id_usr']."'");
    if ($sql->num_rows > 0) {
      $row = $sql->fetch_assoc();
      $resultados[] = array("success"=> true, "carrito"=> carrito_carga_actual($row["id_per"]));
    }
  } else {
    $resultados[] = array("success"=> false, "message" => "Auth error");
  }
  print json_encode($resultados);
  include_once('../../functions/cierra_conexion.php');
?>
