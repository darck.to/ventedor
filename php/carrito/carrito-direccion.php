<?php
  session_start();
  header("Access-Control-Allow-Origin: *");
	header('Content-type: application/json');
  include_once('../../functions/functions.php');
  include_once('../../functions/productos_functions.php');
  include_once('../../functions/carrito_functions.php');
  include_once('../../functions/abre_conexion.php');

  //$nav = mysqli_real_escape_string($mysqli,$_POST['nav']);
  $auth = mysqli_real_escape_string($mysqli,$_POST['auth']);
  $user = mysqli_real_escape_string($mysqli,$_POST['user']);

  date_default_timezone_set("America/Mexico_City");
  $fechaActual = Date('Y-m-d H:i:s');

  $resultados = array();

  $sql = $mysqli->query("SELECT id_usr FROM auth_table WHERE init_index = '".$auth."' AND nom = '".$user."' ");
  if ($sql->num_rows > 0) {
    $row = $sql->fetch_assoc();
    $sql = $mysqli->query("SELECT id_per, nom, ape, apm, tel, cel, mai FROM perf_table WHERE id_usr = '".$row['id_usr']."'");
    //lee usuario comprador
    if ($sql->num_rows > 0) {
      $row = $sql->fetch_assoc();
      $perfil = $row['id_per'];
      $sqd = $mysqli->query("SELECT cal, num, col, cp, ciu, est, pai, ref FROM dire_table WHERE id_per = '".$perfil."'");
      //lee usuario comprador
      if ($sqd->num_rows > 0) {
        $rod = $sqd->fetch_assoc();
        $resultados[] = array("success" => true,"nombre" => $row['nom'], "paterno" => $row['ape'], "materno" => $row['apm'], "telefono" => $row['tel'], "celular" => $row['cel'], "mail" => $row['mai'], "calle" => $rod['cal'], "numero" => $rod['num'], "colonia" => $rod['col'], "cp" => $rod['cp'], "ciudad" => $rod['ciu'], "estado" => $rod['est'], "pais" => $rod['pai'], "referencia" => $rod['ref']);
      }
    }
  } else {
    $resultados[] = array("success"=> false, "message" => "Auth error", "carrito" => $carrito);
  }

  print json_encode($resultados);
  include_once('../../functions/cierra_conexion.php');
?>
