<?php
  session_start();
  header("Access-Control-Allow-Origin: *");
	header('Content-type: application/json');
  include_once('../../functions/productos_functions.php');
  include_once('../../functions/abre_conexion.php');

  $resultados = array();

  $sqt = $mysqli->query("SELECT id_tie, id_per, nom FROM tien_table");
  if ($sqt->num_rows > 0) {
    while ($rot = $sqt->fetch_assoc()) {
      $resultados[] = array("success"=> true, "tienda"=> true, "nombre"=> $rot['nom'], "id_tie" => $rot['id_tie']);
      $sqpr = $mysqli->query("SELECT id_pro FROM prod_table WHERE id_tie = '".$rot['id_tie']."'");
      if ($sqpr->num_rows > 0) {
        while ($rpr = $sqpr->fetch_assoc()) {
          $name[] = array("key"=> "nombre");
          $nombre = productos_obtener_value($rot['id_per'], $rot['id_tie'], $rpr['id_pro'], $name);
          $price[] = array("key"=> "precio");
          $precio = productos_obtener_value($rot['id_per'], $rot['id_tie'], $rpr['id_pro'], $price);
          $resultados[] = array("success"=> true, "producto"=> true, "nombre"=> $nombre, "precio"=> $precio, "id_pro" => $rpr['id_pro'], "id_tie" => $rot['id_tie']);
        }
      }
    }
  } else {
    $resultados[] = array("success"=> false, "message" => "Auth error");
  }
  print json_encode($resultados);
  include_once('../../functions/cierra_conexion.php');
?>
