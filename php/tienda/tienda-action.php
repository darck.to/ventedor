<?php
  session_start();
  header("Access-Control-Allow-Origin: *");
	header('Content-type: application/json');
  include_once('../../functions/functions.php');
  include_once('../../functions/tienda_functions.php');
  include_once('../../functions/abre_conexion.php');

  //$nav = mysqli_real_escape_string($mysqli,$_POST['nav']);
  $auth = mysqli_real_escape_string($mysqli,$_POST['auth']);
  $user = mysqli_real_escape_string($mysqli,$_POST['user']);

  date_default_timezone_set("America/Mexico_City");
  $fechaActual = Date('Y-m-d H:i:s');

  $resultados = array();

  $sql = $mysqli->query("SELECT id_usr FROM auth_table WHERE init_index = '".$auth."' AND nom = '".$user."' ");
  if ($sql->num_rows > 0) {
    $row = $sql->fetch_assoc();
    $sqp = $mysqli->query("SELECT id_per FROM perf_table WHERE id_usr = '".$row['id_usr']."'");
    if ($sqp->num_rows > 0) {
      $rop = $sqp->fetch_assoc();
      //lee accion entrante
      $action = mysqli_real_escape_string($mysqli,$_POST['action']);
      if ($action == 0) {
        $nombre = mysqli_real_escape_string($mysqli,$_POST['nombre']);
        $id_pt = generateRandomString(8) . "_TIE";
        if ($sql = $mysqli->query("INSERT INTO tien_table (nom, fec, id_per, id_tie) VALUES ('".$nombre."', '".$fechaActual."', '".$rop['id_per']."', '".$id_pt."')")) {
          //si se crea la esctructura de archivos de la tienda, puede proseguir, si no borra la tienda de la bdd
          if (tienda_crea_estructura($rop['id_per'], $id_pt)) {
            $content[] = array("id_tie"=> $id_pt, "id_per"=> $rop['id_per'], "nombre"=> $nombre, "fecha_m"=> $fechaActual);
            if (tienda_configuracion_json($action, $rop['id_per'], $id_pt, $content)) {
              $resultados[] = array("success"=> true, "message"=> "Alta de tienda");
            }
          } else {
            if ($mysqli->query("DELETE FROM tien_table WHERE id_per = '".$rop['id_per']."' AND id_tie = '".$id_pt."'")) {
              $resultados[] = array("success"=> false, "message"=> "Falló creación de estructura en alta de tienda, se eliminó el registro");
            }
          }
        } else {
          $resultados[] = array("success"=> false, "message"=> "Falló alta de tienda");
        }
      } else if ($action == 1) {
        $nombre = mysqli_real_escape_string($mysqli,$_POST['nombre']);
        $value = mysqli_real_escape_string($mysqli,$_POST['value']);
        if ($sql = $mysqli->query("UPDATE tien_table SET nom = '".$nombre."' WHERE id_per = '".$rop['id_per']."' AND id_tie = '".$value."'")) {
          $content[] = array("key"=> "nombre", "value"=> $nombre);
          if (tienda_configuracion_json($action, $rop['id_per'], $value, $content)) {
            $resultados[] = array("success"=> true, "message"=> "Modificación de tienda");
          }
        } else {
          $resultados[] = array("success"=> false, "message"=> "Falló modificación de tienda");
        }
      } else if ($action == 2) {
        $value = mysqli_real_escape_string($mysqli,$_POST['value']);
        if (tienda_borra_estructura($rop['id_per'], $value)) {
          if ($sql = $mysqli->query("DELETE FROM tien_table WHERE id_per = '".$rop['id_per']."' AND id_tie = '".$value."'")) {
            $resultados[] = array("success"=> true, "message"=> "Tienda eliminada");
          } else {
            $resultados[] = array("success"=> false, "message"=> "Falló la eliminación de tienda");
          }
        } else {
          $resultados[] = array("success"=> false, "message"=> "Falló la eliminación de le estructura de directorios de la tienda");
        }
      }

    }
  } else {
    $resultados[] = array("success"=> false, "message" => "Auth error");
  }

  print json_encode($resultados);
  include_once('../../functions/cierra_conexion.php');
?>
