<?php
  session_start();
  header("Access-Control-Allow-Origin: *");
	header('Content-type: application/json');
  include_once('../../functions/abre_conexion.php');

  $nav = mysqli_real_escape_string($mysqli,$_POST['nav']);
  $auth = mysqli_real_escape_string($mysqli,$_POST['auth']);
  $user = mysqli_real_escape_string($mysqli,$_POST['user']);

  $resultados = array();

  $sql = $mysqli->query("SELECT id FROM auth_table WHERE init_index = '".$auth."' AND nom = '".$user."' ");
  if ($sql->num_rows > 0) {
    //case para destino de navegacion admin
    switch ($nav) {
      case "dashboard":
      $resultados[] = array("success"=> true, "path"=> "templates/admin/admin-dashboard.html", "nav"=> $nav);
      break;
      case "perfil":
      $resultados[] = array("success"=> true, "path"=> "templates/admin/admin-perfil.html", "nav"=> $nav);
      break;
      case "tienda":
      $resultados[] = array("success"=> true, "path"=> "templates/admin/admin-tienda.html", "nav"=> $nav);
      break;
      case "productos":
      $resultados[] = array("success"=> true, "path"=> "templates/admin/admin-productos.html", "nav"=> $nav);
      break;
      case "ventas":
      $resultados[] = array("success"=> true, "path"=> "templates/admin/admin-ventas.html", "nav"=> $nav);
      break;
      case "compras":
      $resultados[] = array("success"=> true, "path"=> "templates/admin/admin-compras.html", "nav"=> $nav);
      break;

      default:
      $resultados[] = array("success"=> false, "message"=> "Error de selección de menú");
    }
  } else {
    $resultados[] = array("success"=> false, "message" => "Auth error");
  }
  print json_encode($resultados);
  include_once('../../functions/cierra_conexion.php');
?>
