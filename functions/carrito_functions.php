<?php
  function carrito_crea($comprador, $vendedor, $carrito, $content) {
    //CARPETA DE TIENDA Y PRODUCTOS
    $file = '../../data/usr/' . $comprador . '/carritos/' . $carrito . '.json';
    $json = fopen($file, 'w') or die ("error de lectura");
    $newJsonString = json_encode($content, JSON_PRETTY_PRINT);
    file_put_contents($file, $newJsonString);
    fclose($json);
    chmod($file, 0777);
    $tie = true;
    if ($tie) {
      return true;
    } else {
      return false;
    }
  }
  function carrito_borra_estructura($usuario, $tienda) {
    //CARPETA DE TIENDA Y PRODUCTOS
    $dir = '../../data/usr/' . $usuario . '/carritos/' . $carrito . '/';
    $it = new RecursiveDirectoryIterator($dir, RecursiveDirectoryIterator::SKIP_DOTS);
    $files = new RecursiveIteratorIterator($it, RecursiveIteratorIterator::CHILD_FIRST);
    foreach($files as $file) {
      if ($file->isDir()){
        rmdir($file->getRealPath());
      } else {
        unlink($file->getRealPath());
      }
    }
    if (rmdir($dir)) {
      $tie = true;
    }
    if ($tie) {
      return true;
    } else {
      return false;
    }
  }
  function carrito_modifica($comprador, $carrito, $producto) {
    //FUNCTION PARA LA INICIALIZACION Y MODIFICACION DE CONTENIDO A LOS ARCHIVOS DE CONFIGURACION JSON
    $file = '../../data/usr/' . $comprador . '/carritos/' . $carrito . '.json';
    $data = file_get_contents($file);
		$json = json_decode($data, true);
		array_push($json[0]["productos"], $producto);
    $newJsonString = json_encode($json, JSON_PRETTY_PRINT);
    if (file_put_contents($file, $newJsonString)) {
      return true;
    } else {
      return false;
    }
  }
  function carrito_carga_actual($usuario) {
    //FUNCTION PARA LEÉR EL CARRITO DE COMPRAS ACTIVO DEL USUARIO
    $fileList = glob('../../data/usr/' . $usuario . '/carritos/*_CAR.json');
    $returnValue = false;
  	foreach($fileList as $filename){
  		if (file_exists($filename)) {
  			$data = file_get_contents($filename);
  			$json = json_decode($data, true);
  			foreach ($json as $content) {
          if ($content["activo"]){
            $returnValue = $content["id"];
            return $returnValue;
          }
        }
      }
    }
  }
  function carrito_lista($comprador, $carrito) {
    //CARPETA DE TIENDA Y CARRITOS
    $file = '../../data/usr/' . $comprador . '/carritos/' . $carrito . '.json';
    $data = file_get_contents($file);
    $json = json_decode($data, true);
    foreach ($json as &$content) {
      foreach ($content['productos'] as &$productos) {
        $nombreTienda = nombre_tienda($productos['vendedor'], $productos['tienda']);
        $nombreProducto = nombre_producto($productos['vendedor'], $productos['tienda'], $productos['producto']);
        $productos['tienda'] = $nombreTienda;
        $productos['producto'] = $nombreProducto;
      }
    }
    return $json;
  }
  function nombre_tienda($usuario, $tienda) {
    $file = '../../data/usr/' . $usuario . '/store/' . $tienda . '/' . $tienda . '.json';
    $data = file_get_contents($file);
    $json = json_decode($data, true);
    foreach ($json as $content) {
      if ($content['id_tie'] == $tienda) {
        $returnValue = $content['nombre'];
      }
    }
    return $returnValue;
  }
  function nombre_producto($usuario, $tienda, $producto) {
    $file = '../../data/usr/' . $usuario . '/store/' . $tienda . '/' . $producto . '/' . $producto . '.json';
    $data = file_get_contents($file);
    $json = json_decode($data, true);
    foreach ($json as $content) {
      if ($content['id_pro'] == $producto) {
        $returnValue = $content['nombre'];
      }
    }
    return $returnValue;
  }
  function carrito_cupon_flag($cupon) {
    //CARPETA DE CUPONES
    $file = '../../data/cupones/' . $cupon . '_CUP.json';
    if (file_exists($file)) {
      return true;
    } else {
      return false;
    }
  }
  function carrito_cupon_valor($cupon) {
    //CARPETA DE CUPONES
    $file = '../../data/cupones/' . $cupon . '_CUP.json';
    if (file_exists($file)) {
      $data = file_get_contents($file);
      $json = json_decode($data, true);
      foreach ($json as $content) {
        if ($content['id'] == $cupon) {
          $descuento = $content['descuento'];
          $tipo = $content['tipo'];
          $id_descuento = $content['id_descuento'];
          return array($tipo, $descuento, $id_descuento);
        }
      }
    } else {
      return false;
    }
  }
?>
