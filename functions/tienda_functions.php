<?php
  function tienda_crea_estructura($usuario, $tienda) {
    //CARPETA DE TIENDA Y PRODUCTOS
    $dir = '../../data/usr/' . $usuario . '/store/' . $tienda . '/';
    if (mkdir($dir, 0755, true)) {
      $file = '../../data/usr/' . $usuario . '/store/' . $tienda . '/' . $tienda . '.json';
      $json = fopen($file, 'w') or die ("error de lectura");
      fwrite($json, json_encode(array(), JSON_PRETTY_PRINT));
      fclose($json);
      chmod($file, 0777);
      $tie = true;
    }
    if ($tie) {
      return true;
    } else {
      return false;
    }
  }
  function tienda_borra_estructura($usuario, $tienda) {
    //CARPETA DE TIENDA Y PRODUCTOS
    $dir = '../../data/usr/' . $usuario . '/store/' . $tienda . '/';
    $it = new RecursiveDirectoryIterator($dir, RecursiveDirectoryIterator::SKIP_DOTS);
    $files = new RecursiveIteratorIterator($it, RecursiveIteratorIterator::CHILD_FIRST);
    foreach($files as $file) {
      if ($file->isDir()){
        rmdir($file->getRealPath());
      } else {
        unlink($file->getRealPath());
      }
    }
    if (rmdir($dir)) {
      $tie = true;
    }
    if ($tie) {
      return true;
    } else {
      return false;
    }
  }
  function tienda_configuracion_json($action, $usuario, $tienda, $content) {
    //FUNCTION PARA LA INICIALIZACION Y MODIFICACION DE CONTENIDO A LOS ARCHIVOS DE CONFIGURACION JSON
    $file = '../../data/usr/' . $usuario . '/store/' . $tienda . '/' . $tienda . '.json';
    if ($action == 0) {//INICIALIZACION
      $newJsonString = json_encode($content, JSON_PRETTY_PRINT);
      file_put_contents($file, $newJsonString);
      return true;
    } else if ($action == 1) { //MODIFICA EL CONTENIDO DE ACUERDO AL INDICE
      $data = file_get_contents($file);
			$json = json_decode($data, true);
			foreach ($json as $key => &$value) {
				if ($key == $content[0]["key"]) {
          $json[$key][$content[0]["key"]] = $content[0]["value"];
				}
			}
      $newJsonString = json_encode($json, JSON_PRETTY_PRINT);
      file_put_contents($file, $newJsonString);
      return true;
    }
  }
?>
