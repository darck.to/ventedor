$(document).ready(function() {
  // Check for click events on the navbar burger icon
  $(".navbar-burger").click(function() {
    // Toggle the "is-active" class on both the "navbar-burger" and the "navbar-menu"
    $(".navbar-burger").toggleClass("is-active");
    $(".navbar-menu").toggleClass("is-active")
  });

  //revisa login
  $('.check-login').on('click', function(e) {
    var a_href = $(this).attr('href');
    event.preventDefault();
    var stringaUth_key = localStorage.getItem("aUth_key");
    var stringaUth_user = localStorage.getItem("aUth_user");
    var form_data = new FormData();
    form_data.append('auth',stringaUth_key);
    form_data.append('user', stringaUth_user);
    $.ajax({
      type: 'POST',
      url: 'php/init/init-comprueba-auth.php',
      data: form_data,
      cache: false,
      contentType: false,
      processData: false,
      async: true,
      success: function(data) {
        $.each(data, function (name, value) {
          if (value.success) {
            window.location.href = a_href
          } else {
            modal()
            $('.modal-content').load('templates/init/init-form.html')
          }
        })
      },
      error: function(xhr, tst, err) {
        toast(err)
      }
    })
  })
})
