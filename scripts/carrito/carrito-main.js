$(document).ready(function() {

  var carro = carrito(); //obtenemos el carrito actual
  carga_carrito_container(carro);

  function carga_carrito_container(e) {
    //para la cuponera
    $('.input-cupon').attr('car', e);
    //para el precio
    var precio = 0;
    var total = 0;
    var stringaUth_key = localStorage.getItem("aUth_key");
    var stringaUth_user = localStorage.getItem("aUth_user");
    var carrito = e;
    var form_data = new FormData();
    form_data.append('auth',stringaUth_key);
    form_data.append('user', stringaUth_user);
    form_data.append('carrito', carrito)
    $.ajax({
      type: 'POST',
      url: 'php/carrito/carrito-lista.php',
      data: form_data,
      cache: false,
      contentType: false,
      processData: false,
      async: false,
      success: function(data) {
        var box = '';
        $.each(data, function (key, cont) {
          if (cont.success) {
            $.each(cont.carrito, function (name, value) {
              box += '<div class="columns">';
                box += '<div class="column is-8">';
                  var pu = 0;
                    $.each(value.productos, function (id, val) {
                      pu = (parseFloat(val.precio) - (parseFloat(val.precio) * parseFloat(val.descuento)));
                      precio = (precio + parseFloat(pu));
                      total = (total + parseFloat(val.precio));
                      box += '<p>' + val.tienda + '&nbsp;' + val.producto + '&nbsp;Precio con descuento: ' + pu + ' | <small>Precio Normal: ' + val.precio + '</small>&nbsp;<small>Desc: ' + (val.descuento * 100) + '%</small></p>'
                    });
                box += '</div>';
                box += '<div class="column">';
                  box += '<p>Precio con desc: ' + precio.toFixed(2) + '</p>';
                  box += '<p><small>Precio sin desc: ' + total.toFixed(2) + '</small></p>';
                box += '</div>';
              box += '</div>'

            })
          }
        })
        $('#activo-container').html(box);

        //carga direccion
        carrito_direccion(carrito);

      },
      error: function(xhr, tst, err) {
        toast(err)
      }
    })
  }

  function carrito_direccion(carrito) {
    var stringaUth_key = localStorage.getItem("aUth_key");
    var stringaUth_user = localStorage.getItem("aUth_user");
    var form_data = new FormData();
    form_data.append('auth',stringaUth_key);
    form_data.append('user', stringaUth_user)
    $.ajax({
      type: 'POST',
      url: 'php/carrito/carrito-direccion.php',
      data: form_data,
      cache: false,
      contentType: false,
      processData: false,
      async: false,
      success: function(data) {
        var box = '';
        $.each(data, function (name, value) {
          if (value.success) {
            box += '<div class="columns is-multiline pa-half">';
              $.each(value, function (key, content) {
                if (key != "success") {
                  box += '<div class="column is-3 pa-less">';
                    box += '<div class="field">';
                      box += '<div class="control">';
                        box += '<input id="input-' + key +  '" class="input is-small" type="text" placeholder="' + key +  '" value="' + content + '">';
                      box += '</div>';
                    box += '</div>';
                  box += '</div>';
                }
              })
            box += '</div>';
            box += '<button class="button carrito-comprar" val="' + carrito + '">Comprar</button>';
          }
        })
        $('#activo-direccion').html(box);

        //carga metodos de pago
        carrito_pago()

        //procede a la compra
        $('.carrito-comprar').on('click', function(e) {
          var carrito = $(this).attr('val');
          carrito_direccion(carrito)
        })
      },
      error: function(xhr, tst, err) {
        toast(err)
      }
    })
  }

  function carrito_pago(e) {
    
  }

  //muestra cupon input
  $('.agrega-cupon').on('click', function(e) {
    $('.input-cupon').toggleClass('hidden')
  })

  //agrega cupon input
  $('#cupon-input').on('click', function(e) {
    var cupon = $(this).val();
    var carrito = $(this).attr('car');
    var stringaUth_key = localStorage.getItem("aUth_key");
    var stringaUth_user = localStorage.getItem("aUth_user");
    var form_data = new FormData();
    form_data.append('auth',stringaUth_key);
    form_data.append('user', stringaUth_user);
    form_data.append('carrito', carrito)
    form_data.append('cupon', cupon);
    $.ajax({
      type: 'POST',
      url: 'php/carrito/carrito-cupon.php',
      data: form_data,
      cache: false,
      contentType: false,
      processData: false,
      async: false,
      success: function(data) {
        $.each(data, function (name, value) {
          if (value.success) {
            toast(value.message);
            carga_carrito_container(value.carrito)
          }
        })
      },
      error: function(xhr, tst, err) {
        toast(err)
      }
    })
  })

})
