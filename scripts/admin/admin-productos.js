$(document).ready(function() {

  carga_lista_productos();

  function carga_lista_productos(e) {
    $('.productos-list').html('');
    var stringaUth_key = localStorage.getItem("aUth_key");
    var stringaUth_user = localStorage.getItem("aUth_user");
    var form_data = new FormData();
    form_data.append('auth',stringaUth_key);
    form_data.append('user', stringaUth_user);
    //form_data.append('nav', nav)
    $.ajax({
      type: 'POST',
      url: 'php/productos/productos-carga-lista.php',
      data: form_data,
      cache: false,
      contentType: false,
      processData: false,
      async: true,
      success: function(data) {
        var content = '';
        $.each(data, function (name, value) {
          if (value.success) {
            content += '<a class="panel-block">';
              content += '<span class="panel-icon productos-actions" action="1" value="' + value.id_pro + '" tienda="' + value.id_tie + '">';
                content += '<i class="far fa-edit"></i>';
              content += '</span>';
              content += '<span class="panel-icon productos-actions" action="2" value="' + value.id_pro + '" tienda="' + value.id_tie + '">';
                content += '<i class="fas fa-trash-alt"></i>';
              content += '</span>';
              content += 'Producto:&nbsp;' + value.nombre + '&nbsp;|&nbsp;Precio:&nbsp;' + value.precio;
            content += '</a>'
          } else {
            toast(value.message)
          }
          $('.productos-list').html(content)
        })
        //ACTIONS TIENDA
        $('.productos-actions').on('click', function(e){
          var action = $(this).attr('action');
          var value = 0;
          var tienda = 0;
          if (action != 0) {
            value = $(this).attr('value');
            tienda = $(this).attr('tienda');
          };
          productos_actions(action, value, tienda)
        })
      },
      error: function(xhr, tst, err) {
        toast(err)
      }
    })
  }

  //acciones para productos
  function productos_actions(e,v, t) {
    var action = e;
    var value = v;
    var tienda = t;
    var box = '<div class="box">';
    if (action ==  0) {
      modal();
        box += '<section class="section">';
          box += '<h1>Crear producto</h1>';
          box += '<div class="select">';
            box += '<select id="tiendaSelect">';
            box += '</select>';
          box += '</div>';
          box += '<div class="field">';
            box += '<div class="control">';
              box += '<input type="text" id="nombreProducto" class="input" placeholder="Nombre del producto">';
            box += '</div>';
          box += '</div>';
          box += '<div class="field">';
            box += '<div class="control">';
              box += '<input type="number" id="precioProducto" class="input" min="1" step="any">';
            box += '</div>';
          box += '</div>';
          box += '<div class="field">';
            box += '<div class="control">';
              box += '<button class="button productos-go" action=" ' + action +  ' ">Guardar</button>';
            box += '</div>';
          box += '</div>';
        box += '</section>';
      box += '</div>';
      $('.modal-content').html(box);
      var stringaUth_key = localStorage.getItem("aUth_key");
      var stringaUth_user = localStorage.getItem("aUth_user");
      var form_data = new FormData();
      form_data.append('auth',stringaUth_key);
      form_data.append('user', stringaUth_user);
      //form_data.append('nav', nav)
      $.ajax({
        type: 'POST',
        url: 'php/tienda/tienda-carga-lista.php',
        data: form_data,
        cache: false,
        contentType: false,
        processData: false,
        async: true,
        success: function(data) {
          var content = '';
            $.each(data, function (name, value) {
              if (value.success) {
              content += '<option value="' + value.id_tie + '">' + value.nom + '</option>';
            } else {
              toast(value.message)
            }
            $('#tiendaSelect').html(content)
          })
        },
        error: function(xhr, tst, err) {
          toast(err)
        }
      })
    } else if (action ==  1) {
      modal();
        box += '<section class="section">';
          box += '<h1>Edita productos</h1>';
          box += '<div class="field">';
            box += '<div class="control">';
              box += '<input type="text" id="nombreProducto" class="input" placeholder="Nombre del producto">';
            box += '</div>';
          box += '</div>';
          box += '<div class="field">';
            box += '<div class="control">';
              box += '<input type="number" id="precioProducto" class="input" min="1" step="any">';
            box += '</div>';
          box += '</div>';
          box += '<div class="field">';
            box += '<div class="control">';
              box += '<button class="button productos-go" action="' + action + '" value="' + value + '" tienda="' + tienda + '">Editar</button>';
            box += '</div>';
          box += '</div>';
        box += '</section>';
      box += '</div>';
      $('.modal-content').html(box)
    } else if (action ==  2) {
      modal();
        box += '<section class="section">';
          box += '<h1>Borrar producto</h1>';
            box += '<div class="control">';
              box += '<button class="button productos-go is-large is-danger" action="' + action + '" value="' + value + '" tienda="' + tienda + '">Borrar</button>';
            box += '</div>';
          box += '</div>';
        box += '</section>';
      box += '</div>';
      $('.modal-content').html(box)
    }

    //productos go function procesa en el servidor
    $('.productos-go').on('click', function(e){
      var action = $(this).attr('action')
      var stringaUth_key = localStorage.getItem("aUth_key");
      var stringaUth_user = localStorage.getItem("aUth_user");
      var form_data = new FormData();
      form_data.append('auth',stringaUth_key);
      form_data.append('user', stringaUth_user);
      form_data.append('action', action)
      if (action == 0) {
        var tienda = $('#tiendaSelect option:selected').val();
        var nombre = $('#nombreProducto').val();
        var precio = $('#precioProducto').val();
        form_data.append('nombre', nombre);
        form_data.append('precio', precio);
        form_data.append('tienda', tienda)
      } else if (action == 1) {
        var nombre = $('#nombreProducto').val()
        var value = $(this).attr('value');
        var tienda = $(this).attr('tienda');
        var precio = $('#precioProducto').val();
        form_data.append('precio', precio);
        form_data.append('nombre', nombre);
        form_data.append('value', value);
        form_data.append('tienda', tienda)
      } else if (action == 2) {
        var value = $(this).attr('value');
        form_data.append('value', value)
      }
      //form_data.append('nav', nav)
      $.ajax({
        type: 'POST',
        url: 'php/productos/productos-action.php',
        data: form_data,
        cache: false,
        contentType: false,
        processData: false,
        async: true,
        success: function(data) {
          $.each(data, function (name, value) {
            if (value.success) {
              carga_lista_productos();
              xmodal()
            } else {
              toast(value.message)
            }
          })
        },
        error: function(xhr, tst, err) {
          toast(err)
        }
      })
    })
  }
});
