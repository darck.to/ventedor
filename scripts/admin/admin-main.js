$(document).ready(function() {

  //obten la variable de navegacion del menu
  var nav = getUrlGet('nav')
  //carga main precio
  var stringaUth_key = localStorage.getItem("aUth_key");
  var stringaUth_user = localStorage.getItem("aUth_user");
  var form_data = new FormData();
  form_data.append('auth',stringaUth_key);
  form_data.append('user', stringaUth_user);
  form_data.append('nav', nav);
  $.ajax({
    type: 'POST',
    url: 'php/admin/admin-contenedor.php',
    data: form_data,
    cache: false,
    contentType: false,
    processData: false,
    async: true,
    success: function(data) {
      $.each(data, function (name, value) {
        if (value.success) {
          $('#admin-content').load(value.path)
        } else {
          $('#admin-content').load(value.message)
        }
      })
    },
    error: function(xhr, tst, err) {
      toast(err)
    }
  })

  //SESSION CLOSE
  $('.close-login').on('click', function(e){
    //CIERRA SESION PHP
    $.ajax({
      type: 'POST',
      url: 'php/init/init-start.php',
      data: {
        a: 0
      },
      cache: false,
      success: function(data) {
        localStorage.removeItem("aUth_key");
        localStorage.removeItem("aUth_user");
        window.location.href = '.'
      },
      error: function(xhr, tst, err) {
        toast(err)
      }
    })
  })

})
