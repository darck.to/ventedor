$(document).ready(function() {

  carga_lista_tiendas();

  function carga_lista_tiendas(e) {
    $('.tienda-list').html('');
    var stringaUth_key = localStorage.getItem("aUth_key");
    var stringaUth_user = localStorage.getItem("aUth_user");
    var form_data = new FormData();
    form_data.append('auth',stringaUth_key);
    form_data.append('user', stringaUth_user);
    //form_data.append('nav', nav)
    $.ajax({
      type: 'POST',
      url: 'php/tienda/tienda-carga-lista.php',
      data: form_data,
      cache: false,
      contentType: false,
      processData: false,
      async: true,
      success: function(data) {
        var content = '';
          $.each(data, function (name, value) {
            if (value.success) {
            content += '<a class="panel-block">';
              content += '<span class="panel-icon tienda-actions" action="1" value="' + value.id_tie + '">';
                content += '<i class="far fa-edit"></i>';
              content += '</span>';
              content += '<span class="panel-icon tienda-actions" action="2" value="' + value.id_tie + '">';
                content += '<i class="fas fa-trash-alt"></i>';
              content += '</span>';
              content += 'Tienda: ' + value.nom;
            content += '</a>';
          } else {
            toast(value.message)
          }
          $('.tienda-list').html(content)
        })
        //ACTIONS TIENDA
        $('.tienda-actions').on('click', function(e){
          var action = $(this).attr('action');
          var value = 0;
          if (action != 0) {
            value = $(this).attr('value')
          };
          tienda_actions(action, value)
        })
      },
      error: function(xhr, tst, err) {
        toast(err)
      }
    })
  }

  //acciones para tienda
  function tienda_actions(e,v) {
    var action = e;
    var value = v;
    var box = '<div class="box">';
    if (action ==  0) {
      modal();
        box += '<section class="section">';
          box += '<h1>Crear tienda</h1>';
          box += '<div class="field">';
            box += '<div class="control">';
              box += '<input type="text" id="nombreTienda" class="input" placeholder="Nombre de la tienda">';
            box += '</div>';
          box += '</div>';
          box += '<div class="field">';
            box += '<div class="control">';
              box += '<button class="button tienda-go" action=" ' + action +  ' ">Guardar</button>';
            box += '</div>';
          box += '</div>';
        box += '</section>';
      box += '</div>';
      $('.modal-content').html(box)
    } else if (action ==  1) {
      modal();
        box += '<section class="section">';
          box += '<h1>Edita tienda</h1>';
          box += '<div class="field">';
            box += '<div class="control">';
              box += '<input type="text" id="nombreTienda" class="input" placeholder="Nombre de la tienda">';
            box += '</div>';
          box += '</div>';
          box += '<div class="field">';
            box += '<div class="control">';
              box += '<button class="button tienda-go" action="' + action + '" value="' + value + '">Editar</button>';
            box += '</div>';
          box += '</div>';
        box += '</section>';
      box += '</div>';
      $('.modal-content').html(box)
    } else if (action ==  2) {
      modal();
        box += '<section class="section">';
          box += '<h1>Borrar tienda</h1>';
            box += '<div class="control">';
              box += '<button class="button tienda-go is-large is-danger" action="' + action + '" value="' + value + '">Borrar</button>';
            box += '</div>';
          box += '</div>';
        box += '</section>';
      box += '</div>';
      $('.modal-content').html(box)
    }

    //tienda go function procesa en el servidor
    $('.tienda-go').on('click', function(e){
      var action = $(this).attr('action')
      var stringaUth_key = localStorage.getItem("aUth_key");
      var stringaUth_user = localStorage.getItem("aUth_user");
      var form_data = new FormData();
      form_data.append('auth',stringaUth_key);
      form_data.append('user', stringaUth_user);
      form_data.append('action', action)
      if (action == 0) {
        var nombre = $('#nombreTienda').val();
        form_data.append('nombre', nombre)
      } else if (action == 1) {
        var nombre = $('#nombreTienda').val()
        var value = $(this).attr('value');
        form_data.append('nombre', nombre);
        form_data.append('value', value)
      } else if (action == 2) {
        var value = $(this).attr('value');
        form_data.append('value', value)
      }
      //form_data.append('nav', nav)
      $.ajax({
        type: 'POST',
        url: 'php/tienda/tienda-action.php',
        data: form_data,
        cache: false,
        contentType: false,
        processData: false,
        async: true,
        success: function(data) {
          $.each(data, function (name, value) {
            if (value.success) {
              carga_lista_tiendas();
              xmodal()
            } else {
              toast(value.message)
            }
          })
        },
        error: function(xhr, tst, err) {
          toast(err)
        }
      })
    })
  }
});
