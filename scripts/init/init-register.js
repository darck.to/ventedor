$(document).ready(function() {

  $(document).on('focus',':input', function(){
    $(this).attr('autocomplete','off');
  });

  $('.buttonLoginForm').on('click', function(e){
    $('.modal-content').load('templates/init/init-form.html');
  });

  //REGISTRAMOS EL USUARIO
  $('#initRegisterUser').submit(function(event) {
    event.preventDefault();

    var formData = new FormData(document.getElementById("initRegisterUser"));
    var formMethod = $(this).attr('method');
    var rutaScrtip = $(this).attr('action');

    var request = $.ajax({
      url: rutaScrtip,
      method: formMethod,
      data: formData,
      contentType: false,
      processData: false,
      async:true,
      dataType : 'json',
      crossDomain: true,
      context: document.body,
      cache: false,
    });
    // handle the responses
    request.done(function(data) {
      var aprobacion, key, user;
      $.each(data, function (name, value) {
        aprobacion = value.success;
        key = value.aUth_key;
        user = value.aUth_user;
      });
      if (aprobacion == true) {
        $('.signButton').addClass('is-loading');
        localStorage.setItem("aUth_key", key);
        localStorage.setItem("aUth_user", user);
        carga_session()
      } else {
        toast(data.error);
        console.log(data.error)
      }
    })
    request.fail(function(jqXHR, textStatus) {
      console.log(textStatus);
    })
    request.always(function(data) {
      // clear the form
      $('#initRegisterUser').trigger('reset');
    })

    function carga_session(e) {
      $.ajax({
        type: 'POST',
        url: 'php/init/init-start.php',
        data: {
          a: 1
        },
        cache: false,
        success: function(data) {
          window.location.href = 'admin.html'
        },
        error: function(xhr, tst, err) {
          console.log(err);
        }
      })
    }

  })

});

//EMAIL
$('#r-mai').on('keyup', function() {
	email = $(this).val();
	if (validateEmail(email)) {
    $('.signButton').prop('disabled',false);
	} else {
    $('.signButton').prop('disabled',true);
	}

  if (this.value.length == 0) {
    $('.signButton').prop('disabled',true);
  }
});

//CONFIMATION
$('#r-pac').on('keyup', function() {
	pass = $('#r-pas').val();
	confirm = $(this).val();
	if (confirm != pass) {
    $('.signButton').prop('disabled',true);
	} else {
    $('.signButton').prop('disabled',false);
	}

  if (this.value.length == 0) {
  	$('.signButton').prop('disabled',true);
  }
})

function validateEmail(sEmail) {
	var filter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
	if (filter.test(sEmail)) {
		return true;
	}
	else {
		return false;
	}
}
