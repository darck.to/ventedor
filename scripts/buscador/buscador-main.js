$(document).ready(function() {

  carrito();
  buscador_carga_lista();

  function buscador_carga_lista(e) {
    $('#buscadorContainer').html('');
    $.ajax({
      type: 'POST',
      url: 'php/buscador/buscador-carga-lista.php',
      cache: false,
      contentType: false,
      processData: false,
      async: true,
      success: function(data) {
        var content = '';
        $.each(data, function (name, value) {
          if (value.success) {
            sub = 'Tienda';
            ref = 'tienda';
            pro = '';
            addToCart = '';
            if (value.producto) {
              ref = 'producto';
              sub = 'precio: ' + value.precio;
              pro = value.id_pro;
              addToCart = '<i class="far fa-plus-square handed carrito-agregar" tie="' + value.id_tie + '" pro="' + value.id_pro + '"></i>'
            }
            content += '<div class="column is-2">';
              content += '<div class="card">';
                content += '<div class="card-image handed abre-' + ref + '" tie="' + value.id_tie + '" pro="' + pro + '">';
                  content += '<figure class="image is-4by3">';
                    content += '<img src="https://bulma.io/images/placeholders/1280x960.png" alt="Placeholder image">';
                  content += '</figure>';
                content += '</div>';
                content += '<div class="card-content">';
                  content += '<div class="media">';
                    content += '<div class="media-content">';
                      content += '<p class="title is-4">' + value.nombre + '</p>';
                      content += '<p class="subtitle is-6">' + sub + '</p>';
                      content += addToCart;
                    content += '</div>';
                  content += '</div>';
                content += '</div>';
              content += '</div>';
            content += '</div>';
          } else {
            toast(value.message)
          }
          $('#buscadorContainer').html(content)
        })
        //tienda
        $('.abre-tienda').on('click', function(e) {
          tienda($(this).attr('tie'))
        })
        //producto
        $('.abre-producto').on('click', function(e) {
          producto($(this).attr('tie'), $(this).attr('pro'))
        })
        //carrito ad
        $('.carrito-agregar').on('click', function(e) {
          carrito_agr($(this).attr('tie'), $(this).attr('pro'))
        })
      },
      error: function(xhr, tst, err) {
        toast(err)
      }
    })
  }

});
